from tkinter import *
from time import sleep

while True:
    try:
        print('I suggest you to enter >= 70x70 to row and column input')
        row = int(input('rows: '))
        column = int(input('columns: '))
        break
    except:
        print('TypeError, please enter only integers')
        continue


class Simulation:
    def __init__(self, canvas, row, column, width, height):
        self.canvas = canvas
        self.row = row
        self.column = column
        self.width = width
        self.height = height
        self.field = [[self.DEAD for _ in range(self.column)] for _ in range(self.row)]
        self.animate()

        main_middle = int(len(self.field) / 2) if len(self.field) % 2 == 1 else int(len(self.field) / 2) - 1
        nested_middle = int(len(self.field[main_middle]) / 2) if len(self.field) % 2 == 1 else int(len(self.field) /
                                                                                                   2) - 1
        self.field[main_middle][nested_middle] = 1

    ALIVE = 1
    DEAD = 0
    __NEIGHBOURS = ((0, 1), (1, 1), (1, 0), (1, -1), (0, -1), (-1, -1), (-1, 0), (-1, 1))
    # __NEIGHBOURS = ((0, -1), (0, 1), (1, 0), (-1, 0))

    """
    x - column
    y - row
    """

    # Get 4 or 6 neighbours around each cell in matrix (von Neumann neighborhood, Moor neighbourhood)
    # it depends on which variable __NEIGHBOURS you choose, with 4 positions or 8 positions
    def __get_neighbours(self, x, y):
        for dx, dy in self.__NEIGHBOURS:
            yield x + dx, y + dy

    def show_field(self):
        for row_item in self.field:
            print(*row_item)

    # Check is the cell inside of matrix or not
    def __is_cell_alive(self, field, column_neighbour, row_neighbour):
        return 0 <= column_neighbour < self.column \
               and 0 <= row_neighbour < self.row \
               and field[row_neighbour][column_neighbour] == self.ALIVE

    def game(self):
        empty_field = [[0 for _ in range(self.column)] for _ in range(self.row)]

        for row_item in range(self.row):
            for column_item in range(self.column):
                spam = self.field[row_item][column_item]
                alive_neighbour = 0

                for column_neighbour, row_neighbour in self.__get_neighbours(column_item, row_item):
                    if self.__is_cell_alive(self.field, column_neighbour, row_neighbour):
                        alive_neighbour += 1
                    else:
                        alive_neighbour += 0

                # if cell is dead
                if spam == self.DEAD:
                    if alive_neighbour == 1:
                        empty_field[row_item][column_item] = self.ALIVE
                    else:
                        empty_field[row_item][column_item] = self.DEAD

                # if cell is alive
                else:
                    if 0 <= alive_neighbour < 9:
                        empty_field[row_item][column_item] = self.ALIVE

        self.field = empty_field

    def animate(self):
        row_size = self.width // self.row
        column_size = self.height // self.column

        for row_item in range(self.row):
            for column_item in range(self.column):
                if self.field[row_item][column_item] == self.ALIVE:
                    color = 'red'
                else:
                    color = 'grey'
                self.canvas.create_rectangle((row_item + 1) * row_size, (column_item + 1) * column_size, row_item *
                                             row_size, column_item * column_size, fill=color)

        # sleep(1)
        self.game()
        self.canvas.after(100, self.animate)
        print('*' * 50)
        self.show_field()


if __name__ == '__main__':
    root = Tk()
    root.geometry('1200x1000')
    canvas = Canvas(root, width=1200, height=1000)
    canvas.pack()
    field = Simulation(canvas, row, column, 1200, 1000)
    root.mainloop()
