# README #

### Simulation short description ###
* You have to enter the size of the square (for example: 40x40, 50x50, etc)
* We have one cell at the center of the square, that will reproduce (create fractals)
* Algorythm will look to the cell around each cell at the top, bottom, right, left position (von Neumann neighborhood)
* Cell dead cell will become alive if it has 1 alive neighbours
* Cell will survive if 0 <= cell_neighbour < 9

### Screenshot ###
![Scheme](screenshot/img_1.png)
![Scheme](screenshot/img_2.png)

### Updated ###
![Scheme](screenshot/img_3.png)
![Scheme](screenshot/img_4.png)
I decided to add extra optional rules, now you can choose the amount of neighbours variable, with 4 or 8 neighbours.
If you choose variable with 8 you will get rectangle fractals.
Also, I fixed the problem with the resolution of canvas and tkinter window. Now it displays properly.

### Sources ###
* [Клеточные автоматы](https://www.youtube.com/watch?v=FiO6jkNkrb4&t=632s&ab_channel=Onigiri) (fast-forward to 10:18)
* [draw method](https://pastebin.com/ainzgFwG)